#!/bin/bash

BOARD=Parallella

SRCPATH=$(python3 -c "import os; print(os.path.dirname(os.path.realpath('${BASH_SOURCE[0]}')))")
BOARDPATH=$SRCPATH/../boards

. $BOARDPATH/oh/setenv.sh
#petalinux-util --webtalk off

make BOARDDIR=$BOARDPATH BOARDS=$BOARD
