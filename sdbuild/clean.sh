#!/bin/sh

BOARD=Parallella

SRCPATH=$(python3 -c "import os; print(os.path.dirname(os.path.realpath('${BASH_SOURCE[0]}')))")

cd $SRCPATH/build/$BOARD/petalinux_bsp/hardware_project
make clean
