# Copyright (C) 2022 Xilinx, Inc
# SPDX-License-Identifier: BSD-3-Clause

ARCH_Parallella := arm
BSP_Parallella :=
BITSTREAM_Parallella := base/base.bit
FPGA_MANAGER_Parallella := 1

STAGE4_PACKAGES_Parallella := xrt pynq boot_leds ethernet pynq_peripherals sdcardshrink precached_metadata
